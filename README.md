**Purpose of System Monitor**

*TODO*

* features and stuff
* need for viewable diagnostics
* how this works

**Installation**

1. `$ git clone git@bitbucket.org:deanfortier/orion-system-monitor.git`
2. move contents to a src folder within desired catkin_workspace
3. Install dependancies ...
	* `$ sudo apt-get install sysstat ifstat ntpdate`
4. catkin_make

**Usage**

(Will need to change to run on one launch file)

1. roslaunch system_monitor system_monitor.launch
2. roslaunch diagnostic_aggregator aggregator.launch
3. rosrun rqt_robot_monitor rqt_robot_monitor